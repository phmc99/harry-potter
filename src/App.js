import { useEffect, useState } from "react";

import { Characters } from "./components/Characters";
import { WelcomePage } from "./components/WelcomePage";

import "./styles/reset.css";
import "./styles/style.css";

export const App = () => {
  const [init, setInit] = useState(false);
  const [characterList, setCharacterList] = useState([]);
  const [trio, setTrio] = useState([]);

  useEffect(() => {
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => setCharacterList(response));
  }, []);

  const handleTrio = (characterList) => {
    let aux = [];
    let res = [];
    let i = 0;

    while (i < 3) {
      let random = Math.floor(Math.random() * 11);
      if (!aux.includes(characterList[random].house)) {
        aux.push(characterList[random].house);
        res.push(characterList[random]);
        i++;
      }
    }

    setTrio([...res]);
  };

  return (
    <div className="App">
      {init ? (
        <Characters
          characterList={characterList}
          handleTrio={handleTrio}
          trio={trio}
        />
      ) : (
        <WelcomePage
          characterList={characterList}
          handleTrio={handleTrio}
          setInit={setInit}
        />
      )}
    </div>
  );
};
