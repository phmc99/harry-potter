import "./style.css";

export const WelcomePage = ({ characterList, handleTrio, setInit }) => {
  const nextPage = () => {
    setInit(true);
    return handleTrio(characterList);
  };
  return (
    <div className="container">
      <h1>Torneio Tribruxo</h1>
      <p>Clique no botão abaixo para sortear 3 bruxos</p>
      <button onClick={nextPage}>Começar!</button>
    </div>
  );
};
