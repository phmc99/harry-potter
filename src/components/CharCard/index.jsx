import "./style.css";

export const CharCard = ({ name, image, house }) => {
  return (
    <div className={"card"}>
      <h1>{name}</h1>

      <p className={house}>{house}</p>

      <img src={image} alt={name} className={house} />
    </div>
  );
};
