import { CharCard } from "../CharCard";
import "./style.css";

export const Characters = ({ characterList, handleTrio, trio }) => {
  return (
    <>
      <ul>
        {trio.map((item, index) => (
          <li key={index}>
            <CharCard name={item.name} image={item.image} house={item.house} />
          </li>
        ))}
      </ul>
      <button
        onClick={() => {
          handleTrio(characterList);
        }}
        className="again"
      >
        ↻
      </button>
    </>
  );
};
